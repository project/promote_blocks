** Description
The promote_blocks module works in the same was as "promote to front page" works.  The difference
is that the content is promoted with a block.

** Setup
goto 'admin/settings/promote-blocks'


** Theming
Its expected that you should theme your block.  I supplied a very basic theme for now.
The theming function is theme_promote_block($node)

** Credits
Originally coded by Frank Febbraro.
Upgraded to Drupal 5 by Eric Mckenna.

Current maintainer: Eric Mckenna (emckenna AT phase2technology DOT com)
